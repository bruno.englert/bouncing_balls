#ifndef FIZIKA_H_BEILLESZTVE
#define FIZIKA_H_BEILLESZTVE

#include <math.h>
#include "list.h"
#include "menu.h"

void utkozes_fal(Ball *mozgo, SDL_Surface *screen);
double tavolsag(double x1,double x2,double y1,double y2);
void utkozes_labda(Lista *lista, Ball *b1);
void szimulal(Lista *lista,Ball *labda, SDL_Surface *screen);


#endif
