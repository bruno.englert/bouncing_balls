main: list.o main.o menu.o physics.o file.o
	gcc list.o menu.o physics.o file.o main.o -lm -o main `sdl-config --cflags --libs` -lSDL_gfx -lSDL_ttf -lSDL_image -lSDL_mixer

list.o: list.c list.h
	gcc list.c -c -o list.o

menu.o: menu.c menu.h
	gcc menu.c -c -o menu.o

fizika.o: physics.c physics.h
	gcc physics.c -c -o physics.o

file.o: file.c file.h
	gcc file.c -c -o file.o

main.o: main.c lista.h menu.h
	gcc main.c -c -o main.o



clean:
	rm -rf *o main
