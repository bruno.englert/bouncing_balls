/**
 * @file nagyhazi.c 
 * @brief a fő program
 */

/*
Felhasznalt anyag:
https://infoc.eet.bme.hu/sdl.php#3
https://en.wikipedia.org/wiki/Elastic_collision

Megjegyzés: Golyók néha "összeragadnak".

Használat: Jobb klikk: menüben lehet választani a bal felső sarokban.
           Bal klikk: törlés a golyókból
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <SDL/SDL.h>
#include <SDL/SDL_gfxPrimitives.h>
#include <SDL/SDL_image.h>
#include <time.h>
#include "list.h"
#include "menu.h"
#include "physics.h"
#include "file.h"

/* Standard 16:9 screen sizes:
	    640		360	
		864		486	
		960		540	
		1024	576	
		1280	720	
		1366	768	
		1600	900	
		1920	1080 */
		
#define SCREEN_W 1024
#define SCREEN_H 576
		
/* Globális változók inicializálása */
//SDL
SDL_Surface *screen;
SDL_Surface* menu_kep;
//Menü
Menu menu_futas= RUN;
Menu menu_grav=GRAV_be;
Menu menu_air=AIR_be;
Menu menu_opc= UJ;

/* ez a fuggveny hivodik meg az idozito altal.
 * betesz a feldolgozando esemenyek koze (push) egy felhasznaloi esemenyt */
Uint32 idozit(Uint32 ms, void *param) {
    SDL_Event ev;
    ev.type = SDL_USEREVENT;
    SDL_PushEvent(&ev);
    return ms;   /* ujabb varakozas */
}

/**
 * @brief Ez a fügvény rajzolja ki a labdákat és a menüt
 * @param *lista egy listára mutató pointer
 * @author Englert Brunó
 * @date 2014. 12. 7.
 */
void rajzol(Lista *lista){
	Ball *mozgo;
	/* mindent kitörlünk egy nagy fekete téglalappal */
	boxColor(screen, 0, 0, screen->w,screen->h,0x000000FF);
	/* Végigmegyünk a teljes listán */
    for(mozgo=lista->elso->kov; mozgo !=lista->utolso; mozgo=mozgo->kov){
        /* labdák kirajzolása */
		filledCircleRGBA(screen, mozgo->x,  screen->h-mozgo->y, mozgo->r, mozgo->p,mozgo->z,mozgo->k,255);       
	}
    /* A menühöz tartozó képet kirajzoljuk */
	SDL_BlitSurface(menu_kep, NULL, screen, NULL);
	/* A kiválasztott menüpontokhoz kis fehér négyzeteket rajzolunk */
	menu_rajzol(screen);
}

/**
 * @brief Main fügvény
 * @author Englert Brunó
 * @date 2014. 12. 7.
 */
int main(int argc, char *argv[]) {
	/* Inicializális */
    //Random szám a színekhez
    srand(time(0));
    /* SDL inicializálása és ablak megnyitása */
    SDL_Event event; 
    SDL_TimerID id; 
    int quit=0; 
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER);		
    screen = SDL_SetVideoMode(SCREEN_W, SCREEN_H, 0, SDL_ANYFORMAT);
    if (!screen) {
        fprintf(stderr, "Nem sikerult megnyitni az ablakot!\n");
        exit(1);
    }
    SDL_WM_SetCaption("Bouncing Balls", "Bouncing Balls");
    /* Idozito hozzaadasa: 1 ms mulva hivodik meg eloszor */
    id = SDL_AddTimer(10, idozit, NULL);		
    /* Kép betöltése */
    menu_kep = IMG_Load("menu.png");
    if (!menu_kep) {
        fprintf(stderr, "Nem sikerult betolteni a kepfajlt!\n");
        exit(2);
    } 
    /* Egyéb változók inicializálása */
    Ball *tmp=NULL;
	int last_x, last_y, r_x, r_y, click = 0;
	/* Lista létrehozása */
    Lista b=letrehoz();
    /* Ha van file beolvassuk az elemeket a Listába */
	beolvas(&b);
    /* szokasos esemenyhurok, amíg ki nem kapcsoljuk a programot */
    while (!quit) { 		
        SDL_WaitEvent(&event);
        switch (event.type) {
            /* felhasznaloi esemeny: ilyeneket general az idozito fuggveny */
            case SDL_USEREVENT:
				if(click) 
				mozgat(last_x, last_y, tmp, screen);
				szimulal(&b, tmp, screen);
                rajzol(&b);
                if(menu_opc==UJ && click==1)
				filledCircleRGBA(screen, last_x,  last_y, sqrt(tavolsag(r_x, last_x, r_y, last_y)), 255,255,255,100);  
          
				SDL_Flip(screen); 
                break;
            /* egér lenyomása*/
            case SDL_MOUSEBUTTONDOWN:
                if (event.button.button == SDL_BUTTON_LEFT) {
					menu_valtozas(event.button.x, event.button.y);
					if(!((event.button.x<205) && (event.button.y<40))){
					switch (menu_opc){
					case  (UJ):
						r_x=event.button.x;
						r_y=event.button.y;
						click=1;
						break; 
					case (MOZG):
						if(tmp==NULL)
						tmp=kivalszt(event.button.x, event.button.y,&b, screen);
						click = 1;
						break;
						
					case (HUZ):
						if(tmp==NULL)
						tmp=kivalszt(event.button.x, event.button.y,&b,screen);
						click = 1;
						break;
					default:
						break;
					}
					}
                }
                else if (event.button.button == SDL_BUTTON_RIGHT) {
					del_ball(&b,event.button.x,event.button.y, screen);
				}
                
                break;
            /* egér elengedése */
            case SDL_MOUSEBUTTONUP:
                if (event.button.button == SDL_BUTTON_LEFT) {
                    if(!((event.button.x<205) && (event.button.y<40))){
					switch (menu_opc){
					case (UJ):
						beszur(&b,  event.button.x,  screen->h-event.button.y, 0, 0, sqrt(tavolsag(r_x,event.button.x ,r_y,event.button.y)));
						click=0;
						break;
					case (MOZG):
						if(tmp!=NULL){
							tmp->Fx=0;
							tmp->Fy=0;
							tmp=NULL;
							click = 0;
						}
						break;
					case (HUZ):
						if(tmp!=NULL){
							tmp->Fx=0;
							tmp->Fy=0;
							tmp=NULL;
							click = 0;
						}
						break;
					default:
						break;
					}
					}
                }
                break;    
            case SDL_MOUSEMOTION: 
				switch (menu_opc){
					case (UJ):
						if(click==1){
						last_x=event.motion.x;
						last_y=event.motion.y;
						}
					break;
					case (MOZG):
						if(tmp!=NULL){
						last_x=event.motion.x;
						last_y=event.motion.y;
						}
					break;
					case (HUZ):
						if(tmp!=NULL){
						last_x=event.motion.x;
						last_y=event.motion.y;
					default:
					break;
						}
					break;
				}
			break;
					
            case SDL_QUIT:  /* ablak bezarasa */
                quit = 1;
                break;
        }
    }
    /* kiirjuk file-ba a Lista tartalmát */
    kiir(&b);
    /* töröljük a Listát és tartalmát */
    del_list(&b);
    printf("\nSiker");
    /* idozito torlese */
    SDL_RemoveTimer(id);
    SDL_Quit();
    return 0;
}
