#ifndef LISTA_H_BEILLESZTVE
#define LISTA_H_BEILLESZTVE

#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_gfxPrimitives.h>
#include <SDL/SDL_image.h>
#include <math.h>
#include <time.h>


typedef struct Ball {
  double x,y, vx,vy, Fx, Fy, r, m;
  unsigned char p,z,k;
  struct Ball *elozo, *kov;
} Ball;

typedef struct Lista {
  Ball *elso,*utolso;
} Lista;

Lista letrehoz();
int beszur(Lista *lista, double x, double y, double vx, double vy, double r);
void del_list(Lista *lista);
void del_ball(Lista *lista, int x, int y, SDL_Surface *screen);


#endif
