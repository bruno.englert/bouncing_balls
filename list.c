/**
 * @file lista.c 
 * @brief a dublán láncolt strázsás listát kezeli
 */ 

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <SDL/SDL.h>
#include <SDL/SDL_gfxPrimitives.h>
#include <SDL/SDL_image.h>
#include <time.h>
#include "list.h"
#include "menu.h"


/**
 * @brief Létrehozz a strázsákat és össze láncolja őket
 * @return egy pointert add vissza a listához
 * @author Englert Brunó
 * @date 2014. 12. 7.
 */ 
Lista letrehoz(){
	Lista temp;
	temp.elso =(Ball*) malloc(sizeof(Ball));
	temp.utolso =(Ball*) malloc(sizeof(Ball));
	
	temp.elso->kov=temp.utolso;
    temp.utolso->elozo=temp.elso;
	return temp;
}

/**
 * @brief Egy labdát beszúr a lista legelejére
 * @param *lista a listára mutató pointer
 * @param x a labda x koordinátája
 * @param y a labda y koordinátája
 * @param vx a labda sebességének x koordinátája
 * @param vy a labda sebességének y koordinátája
 * @param r a labda sugura
 * @return ha minden rendben ment akkor 0-át ad vissza egyébként 1-et
 * @author Englert Brunó
 * @date 2014. 12. 7.
 */ 
int beszur(Lista *lista, double x, double y, double vx, double vy, double r) {
   Ball *uj;
   uj = (Ball*) malloc(sizeof(Ball));
   if(uj==NULL){
		printf("Err, mermoria");
		return 1;
	}
   //ertek feltoltes
   uj->x=x;
   uj->y=y;
   uj->vx=vx;
   uj->vy=vy;
   uj->Fx=0;
   uj->Fy=0;
   uj->r=r;
   uj->m=3.14*r*r;    //a sugár fugványében adja meg a tömeget
   //színadás
   uj->p=rand()%256;
   uj->z=rand()%256;
   uj->k=rand()%256;
   //beillesztes az elejére
   uj->elozo = lista->elso->kov->elozo;  /* ő a szomszédaira mutat */
   uj->kov = lista->elso->kov;
   lista->elso->kov->elozo = uj; /* a szomszédai rá */
   lista->elso->kov = uj;    
   return 0;
}

/**
 * @brief Kitörli a lista elemeit és a lista strázsáit
 * @param *lista egy poninter ami egy listára mutat
 * @author Englert Brunó
 * @date 2014. 12. 7.
 */ 
void del_list(Lista *lista) {
   Ball *mozgo=lista->elso->kov;
   Ball *tmp;
   while(mozgo!=lista->utolso){
	   tmp=mozgo->kov;
	   free(mozgo);
	   mozgo=tmp;
   }
   free(lista->elso);
   free(lista->utolso);
}

/**
 * @brief Kitöröli azt a labdát ami (sugár+egy adott értek)-nél közelebb van egérhez
 * @param *lista egy poninter ami egy listára mutat
 * @param x az egér x koordinátája
 * @param y az egér y koordinátája
 * @param *screen az "SDL_Surface screen"-re mutató pointer
 * @author Englert Brunó
 * @date 2014. 12. 7.
 */ 
void del_ball(Lista *lista, int x, int y, SDL_Surface *screen){
	Ball *ball;
	int gain=90;
	int tav;
	y=(screen->h)-y;
	
	for(ball=lista->elso->kov; ball !=lista->utolso; ball=ball->kov){
		tav=(ball->y-y)*(ball->y-y)+(ball->x-x)*(ball->x-x);
		tav=sqrt(tav);
		if (tav  <=  ball->r+gain){
			ball->kov->elozo=ball->elozo;
			ball->elozo->kov=ball->kov;
			//free(ball);
		}
	}
}
