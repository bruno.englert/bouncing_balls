#ifndef MENU_H_BEILLESZTVE
#define MENU_H_BEILLESZTVE

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <SDL/SDL.h>
#include <SDL/SDL_gfxPrimitives.h>
#include <SDL/SDL_image.h>
#include <time.h>
#include "list.h"

typedef enum { UJ, GRAV_be, GRAV_ki, AIR_ki, AIR_be, PAUS, RUN, MOZG, HUZ  } Menu;

extern Menu menu_futas;
extern Menu menu_grav;
extern Menu menu_air;
extern Menu menu_opc;

void menu_valtozas(int x, int y);
void menu_rajzol(SDL_Surface *screen);
Ball *kivalszt(int x, int y,Lista *lista, SDL_Surface *screen);
void mozgat(int x, int y, Ball *tmp, SDL_Surface *screen);

#endif

