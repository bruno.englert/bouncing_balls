/**
 * @file fizika.c 
 * @brief a labdák ütközésének fizikáját kezeli
 */

#include <math.h>
#include <SDL/SDL.h>
#include <SDL/SDL_gfxPrimitives.h>
#include "physics.h"
#include "list.h"
#include "menu.h"

#define PI 3.14159265358979323846
double kozell = 0.001;
double rugalmassag = 0.95;


/**
 * @brief A labda és fal közötti ütközéseket kezeli
 * @param *mozgo egy poninter ami egy labdára mutat
 * @param *screen az "SDL_Surface screen"-re mutató pointer
 * @author Englert Brunó
 * @date 2014. 12. 7.
 */ 
void utkozes_fal(Ball *mozgo, SDL_Surface *screen){
                //visszapattanás
				if ( mozgo->x <  mozgo->r  ) {
					mozgo->x += (mozgo->r - mozgo->x);
					if(mozgo->vx<0)
						mozgo->vx *= -1; 
				}
                if (mozgo->y  <  mozgo->r ){
					mozgo->y += (mozgo->r -mozgo->y);
					if(mozgo->vy<0)
						mozgo->vy *= -1;  
                }
                if ( mozgo->x  >  screen->w-mozgo->r  ) {
					mozgo->x -= mozgo->r-(screen->w - mozgo->x) ;
					if (mozgo->vx>0)
						mozgo->vx *= -1; 
				}
                if ( mozgo->y  >  screen->h-mozgo->r){
					mozgo->y -= mozgo->r-(screen->h - mozgo->y) ;
					if(mozgo->vy>0)
						mozgo->vy *= -1;  
               }
}

/**
 * @brief Két pont közötti távolságát határozza meg
 * @param x1 az első pont x koordinátája
 * @param y1 az első pont y koordinátája
 * @param x2 a második pont x koordinátája
 * @param y2 a második pont y koordinátája
 * @return A távolság négyzete. (Néhány helyen a programban célszerübb volt a négyztettel számloni)
 * @author Englert Brunó
 * @date 2014. 12. 7.
 */ 
double tavolsag(double x1,double x2,double y1,double y2){
	x1-=x2;
	y1-=y2;
	return x1*x1+y1*y1;
}

/**
 * @brief Két labda közötti ütközéseket kezeli
 * @param *lista egy poninter ami egy listára mutat
 * @param *b1 egy labdára mutató pointer
 * @author Englert Brunó
 * @date 2014. 12. 7.
 */ 
void utkozes_labda(Lista *lista, Ball *b1){
	double tav, erinto, v1_alfa, v2_alfa, v_1, v_2, v_tmp , r_sum, r_sqr, tot_m; 
	Ball *b2;
           for(b2=b1->kov; b2 !=lista->utolso; b2=b2->kov){
                /* kiszamitjuk a tavot ket golyó kozott */
                tav = tavolsag(b1->x,b2->x,b1->y,b2->y);
                
                r_sum=b1->r+b2->r;
				r_sqr=r_sum*r_sum;
				
				
				
                if ( tav<=r_sqr ){		
					tot_m=b1->m+b2->m;
					erinto = atan2(b1->y-b2->y,b1->x-b2->x)-PI/2;
					v1_alfa = atan2(b1->vy,b1->vx);
					v2_alfa = atan2(b2->vy,b2->vx);
					
					v_1 = (b1->vx)*(b1->vx)+(b1->vy)*(b1->vy);
					v_1 = sqrt(v_1);
					v_tmp=v_1;
					
					v_2 = (b2->vx)*(b2->vx)+(b2->vy)*(b2->vy);
					v_2 = sqrt(v_2);
					
					v_1=(v_1*(b1->m)+(b2->m)*v_2)/tot_m;
					v_2=(v_2*(b2->m)+(b1->m)*v_tmp)/tot_m;
	
					b1->vx=v_2*cos(-v1_alfa+erinto*2)*rugalmassag;
					b1->vy=v_2*sin(-v1_alfa+erinto*2)*rugalmassag;
					////////////////
					b2->vx=v_1*cos(-v2_alfa+erinto*2)*rugalmassag;
					b2->vy=v_1*sin(-v2_alfa+erinto*2)*rugalmassag;
					
					tav=sqrt(tav);
					erinto+=PI/2;
					
					
					b1->x += cos(erinto)*(r_sum-tav+2)/2;
					b1->y += sin(erinto)*(r_sum-tav+2)/2;
					b2->x -= cos(erinto)*(r_sum-tav+2)/2;
					b2->y -= sin(erinto)*(r_sum-tav+2)/2;	
			
			}
		}        
}

/**
 * @brief Kiszámolja a labdák új pozicióját, annak megfelelően hogy mi van kiválasztva a menüben.
 * @param *lista egy poninter ami egy listára mutat
 * @param *labda egy labdára mutató pointer
 * @param *screen az "SDL_Surface screen"-re mutató pointer
 * @author Englert Brunó
 * @date 2014. 12. 7.
 */
void szimulal(Lista *lista,Ball *labda, SDL_Surface *screen ){
	Ball *mozgo;
	for(mozgo=lista->elso->kov; mozgo !=lista->utolso; mozgo=mozgo->kov){
                /* kiszamitjuk az uj helyet */
                if(menu_futas==RUN ){
					
				if(menu_grav==GRAV_be)
				mozgo->vy-=0.1;	
					
				(mozgo->vx) +=mozgo->Fx;
				(mozgo->vy) +=mozgo->Fy;
                
				mozgo->x += mozgo->vx;
                mozgo->y += mozgo->vy;
                
                if(menu_air==AIR_be){
				if((mozgo->Fx)>0)
                mozgo->vx-=kozell*(mozgo->vx)*(mozgo->vx);
                else
                mozgo->vx+=kozell*(mozgo->vx)*(mozgo->vx);
                if((mozgo->Fy)>0)
                mozgo->vy-=kozell*(mozgo->vy)*(mozgo->vy);
                else
                mozgo->vy+=kozell*(mozgo->vy)*(mozgo->vy);
				}
			
                utkozes_fal(mozgo, screen);
                utkozes_labda(lista,mozgo);
				}      
		}
}
