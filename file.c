/**
 * @file file.c 
 * @brief a file-ba irást és a file-ból olvasást kezeli
 */

#include <stdio.h>
#include "list.h"
#include "file.h"

/**
 * @brief Ha léteik, akkor beolvas egy fájlt és a meglévő listát feltölti a fájlban lévő labdákkal
 * @param *lista egy listára mutató pointer
 * @author Englert Brunó
 * @date 2014. 12. 7.
 */ 
void beolvas(Lista *lista){
	FILE *fr;
	fr = fopen("adat.dat", "rb");
	if (fr == NULL) 
		perror("adat.dat megnyitása sikertelen");
	else{
	Ball *uj=(Ball*) malloc(sizeof(Ball));
	while(fread(uj, sizeof(Ball), 1, fr)>0){
			beszur(lista,  0,0, 0, 0, 0);
			    lista->elso->kov->x=uj->x;
				lista->elso->kov->y=uj->y;
				lista->elso->kov->vx=uj->vx;
				lista->elso->kov->vy=uj->vy;
				lista->elso->kov->Fx=uj->Fx;
				lista->elso->kov->Fy=uj->Fy;
				lista->elso->kov->r=uj->r;
				lista->elso->kov->m=uj->m;   
				//színadás
				lista->elso->kov->p=uj->p;
				lista->elso->kov->z=uj->z;
				lista->elso->kov->k=uj->k;       
	   }
	  free(uj);
	fclose(fr);
  }
}


/**
 * @brief Fájlba kiírja a lista tartalmát a program bezárásakor
 * @param *lista egy listára mutató pointer
 * @author Englert Brunó
 * @date 2014. 12. 7.
 */
void kiir( Lista *lista){
	FILE *fw; 
    Ball *mozgo;
    fw = fopen("adat.dat", "wb");
    for(mozgo=lista->elso->kov; mozgo!=lista->utolso; mozgo=mozgo->kov){
           fwrite(mozgo, sizeof(Ball), 1, fw);      
	}
	fclose(fw);
}
