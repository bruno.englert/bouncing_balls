/**
 * @file menu.c 
 * @brief a bal felső sarokban levő menüt kezeli
 */ 

#include <stdio.h>
#include <math.h>
#include <SDL/SDL.h>
#include <SDL/SDL_gfxPrimitives.h>
#include <SDL/SDL_image.h>
#include <time.h>
#include "list.h"
#include "menu.h"
#include "physics.h"


/**
 * @brief A beadott x és y koordiánaták alapján megállapítja hogy volt-e változás és az alapján átírja a menühüz tartozó globális változókat.
 * @param x az egér x koordinátája
 * @param y az egér y koordinátája
 * @author Englert Brunó
 * @date 2014. 12. 7.
 */ 
void menu_valtozas(int x, int y){
	int x_elso=6;
	
	if ( 6 <y &&  y< 6+30){
		if (x_elso<x && x< x_elso+30){
			if(menu_futas == RUN)
				menu_futas = PAUS;
			else
				menu_futas = RUN;
	}		
		
		x_elso+=32;
		if (x_elso<x && x<x_elso+30 ){
			menu_opc = MOZG;
		}
		x_elso+=32;
		if (x_elso<x && x<x_elso+30 ){
			menu_opc = HUZ;
		}
		x_elso+=32;
		if (x_elso<x && x<x_elso+30 ){
			menu_opc = UJ;
		}
		x_elso+=32;
		if (x_elso<x && x<x_elso+30 ){
			if(menu_grav == GRAV_be)
				menu_grav = GRAV_ki;
			else
				menu_grav = GRAV_be;
			
		}
		x_elso+=32;
		if (x_elso<x && x<x_elso+30 ){
			if(menu_air == AIR_be)
				menu_air = AIR_ki;
			else
				menu_air = AIR_be;
		}
		x_elso+=32;
		if (x_elso<x && x<x_elso+30 ){
			
		}
	}		
}

/**
 * Kirajzolja a menüt.
 * @param *screen az "SDL_Surface screen"-re mutató pointer
 * @author Englert Brunó
 * @date 2014. 12. 7.
 */ 
void menu_rajzol(SDL_Surface *screen){
			switch(menu_opc){
			case MOZG:
				rectangleRGBA(screen, 36, 6, 66, 36, 255, 255, 255, 255);
				break;
			case HUZ:
				rectangleRGBA(screen, 68, 6, 98, 36, 255, 255, 255, 255);
				break;
			case UJ:
				rectangleRGBA(screen, 100, 6, 130, 36, 255, 255, 255, 255);
				break;
			default:
				break;		
		}
		 
			if (menu_air==AIR_be)
				rectangleRGBA(screen, 164, 6, 194, 36, 255, 255, 255, 255);
			if (menu_grav==GRAV_be)
				rectangleRGBA(screen, 132, 6, 162, 36, 255, 255, 255, 255);
			if (menu_futas==RUN)
				rectangleRGBA(screen, 6, 6, 36, 36, 255, 255, 255, 255);
}

/**
 * Kattintásra kiválaszt egy labdát
 * @param x az egér x koordinátája
 * @param y az egér y koordinátája
 * @param *lista egy Listára mutató pointer 
 * @param *screen az "SDL_Surface screen"-re mutató pointer
 * @author Englert Brunó
 * @date 2014. 12. 7.
 */ 
Ball *kivalszt(int x, int y,Lista *lista, SDL_Surface *screen){
	Ball *tmp;
	double tav;
	for(tmp=lista->elso; tmp!=lista->utolso; tmp=tmp->kov){
		tav=sqrt(tavolsag((tmp->x), x,(screen->h)-(tmp->y),y));         
		if(tav<(tmp->r))
			return tmp;
	}
	return NULL;
}

/**
 * Mozgatunk egy labdát.
 * @param x az egér x koordinátája
 * @param y az egér y koordinátája
 * @param *tmp egy kiválasztott labdára mutató pointer
 * @param *screen az "SDL_Surface screen"-re mutató pointer
 * @author Englert Brunó
 * @date 2014. 12. 7.
 */ 
void mozgat(int x, int y, Ball *tmp, SDL_Surface *screen){
	double szorzo;
	if(tmp!=NULL){
		if(menu_opc==MOZG){ //Ha mozgatjuk a labbdát akkor nagyobb szorzóval és nagyobb csillapítással dolgozunk így olyan mintha folyamatosan a egéren lenne a labda
		szorzo=0.6;
		tmp->vx*=0.1;
		tmp->vy*=0.1;
		}else{ //Ha a menu_opc nem MOZG akkor menu_opc==HUZ. Ilyenkor kisseb szorzóval, és kissebb csillapítással módosítjuk a labda értékeit (sebesség és ráható erők)
		szorzo=0.06;
		tmp->vx*=0.9;
		tmp->vy*=0.9;
		}	
	tmp->Fx=(x-(tmp->x))*szorzo;
	tmp->Fy=((-y+(screen->h)-(tmp->y)))*szorzo;
	}
}
