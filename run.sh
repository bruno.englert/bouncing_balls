#!/bin/bash

if [ $(dpkg-query -W -f='${Status}' libsdl-gfx1.2-dev 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
   sudo apt install libsdl-gfx1.2-dev
fi

if [ $(dpkg-query -W -f='${Status}' libsdl1.2-dev 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
   sudo apt install libsdl1.2-dev
fi

if [ $(dpkg-query -W -f='${Status}' libsdl-mixer1.2-dev 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
   sudo apt install libsdl-mixer1.2-dev
fi

if [ $(dpkg-query -W -f='${Status}' libsdl-ttf2.0-dev 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
   sudo apt install libsdl-ttf2.0-dev 
fi

if [ $(dpkg-query -W -f='${Status}' libsdl-image1.2-dev  2>/dev/null | grep -c "ok installed") -eq 0 ]; then
   sudo apt install libsdl-image1.2-dev 
fi

make clean
make main
chmod +x ./main
./main
